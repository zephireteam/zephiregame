﻿using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

	private Transform MyTransform;

	// Use this for initialization
	void Start () {
		MyTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (MyTransform.position.y < -10)
			Application.LoadLevel("Dream");
	}
}
