﻿//выбор цели
using UnityEngine;
using System.Collections;
//устанавливаем дополнительную систему для возможности работы с листингами
using System.Collections.Generic;

public class TargettingExt : MonoBehaviour {
	//хранилище всех врагов
	public List <Transform> targets;
	
	//внутренняя переменная для хранения текущей цели
	public Transform selectedTarget;

	//расстояние от игрока до цели
	public int distance;

	public Pawn targInfo;
	private MainHUDClass _mainHUD;

	private Transform MyTransform;
	
	// Use this for initialization
	void Start () {
		//назначаем пустой листинг
		targets = new List<Transform> ();
		MyTransform = transform;
		AddAllEnemies ();
		_mainHUD = GameObject.Find ("__MainHUD").GetComponent<MainHUDClass> ();
		if (_mainHUD == null) Debug.Log("Can't find object __MainHUD");

		//назначаем distance
		if (distance == 0) {
			distance = 30;		
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Tab)) {
				//выбираем цель Tabом
				TargetEnemy();
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			DeSelectTarget();
			selectedTarget = null;
		}
	}
	
	//поиск и добавление все противников в список
	public void AddAllEnemies(){
		//помещаем всех врагов в массив go
		GameObject[] go = GameObject.FindGameObjectsWithTag("Enemy");
		//каждый элемент из найденных заносим в массив потенциальных целей
		foreach (GameObject enemy in go) 
			AddTarget(enemy.transform);
	}

	//if (Vector3.Distance (MyTransform.position, targets[0].position) < 40)

	//добавление в массив очередного элемента
	public void AddTarget(Transform enemy){
		targets.Add (enemy);
	}
	
	//выбор конкретной цели из списка
	private void TargetEnemy(){

		//если ни один не был выбран, выбираем наиближайщий
		if (selectedTarget == null){
			targets.Remove (selectedTarget);
			SortTargetByDistance();
			//проверка на расстояние для targetting для 0 элемента
			if (Vector3.Distance (MyTransform.position, targets[0].position) < distance){
				selectedTarget = targets[0];
			}
			else {
				selectedTarget = null;
			}
		}
		else {
			//иначе - перебор от первого до последнего
			int index = targets.IndexOf(selectedTarget);
			if (index < targets.Count-1){
				index++;
			}
			else {
				index = 0;
			}
			//проверка на расстояние для targetting
			if (Vector3.Distance (MyTransform.position, targets[index].position) < distance){
			//снимаем пометку с текущей цели
			DeSelectTarget();
			//выбираем следующую
			selectedTarget = targets[index];
			}
		}
		//проверка на пустоту
		if (selectedTarget != null) {
			SelectTarget();	
		}
	}
	
	//Сортировка списка по расстоянию до игрока
	private void SortTargetByDistance(){
		targets.Sort (delegate(Transform t1, Transform t2) {
			//сравнивается расстояние между объектом t1 и игроком и расстояние между объектом t2 и игроком
			return Vector3.Distance(t1.position,MyTransform.position).CompareTo(Vector3.Distance(t2.position,MyTransform.position));
		});
	}

	//метим выбранный элемент
	public void SelectTarget(){
		selectedTarget.FindChild ("Plane").renderer.material.color = Color.white;
		targInfo = selectedTarget.GetComponent<Pawn>();
		_mainHUD.ShowEnemyHealthBars (true, targInfo.GetProcHealth());
		GetComponent<PlayerAttackExt> ().target = selectedTarget.gameObject;
	}
	
	//убираем пометку выбранной цели
	public void DeSelectTarget(){
		if (selectedTarget != null) {
						selectedTarget.FindChild ("Plane").renderer.material.color = Color.clear;
						GetComponent<PlayerAttackExt> ().target = null;
				}
		_mainHUD.ShowEnemyHealthBars (false, 0);
	}
}