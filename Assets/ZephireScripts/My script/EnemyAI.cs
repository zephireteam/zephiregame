﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	public Transform target;	//цель
	public int move_speed;		//скорость передвижения
	public int rotation_speed;	//скорость поворота
	public int min_distance;	//расстояние до игрока
	public int max_distance;	//область видимости

	private Transform MyTransform;	//временная переменная для хранения
	//ссылки свойства transform (оптимизация)
	//MyTransform = Enemy
	
	
	//исполняется до наступления Start
	void Awake(){
		//ссылаемся на свойство transform,
		//чтобы сократить время обращения к нему в теле цикла
		MyTransform = transform;
	}
	// Use this for initialization
	void Start () {
		//ищем объект по тегу Player
		GameObject go = GameObject.FindGameObjectWithTag("Player");
		//и делаем нашей целью
		target = go.transform;
		min_distance = 1;	//устанавливаем минимальное расстояние
		max_distance = 20;	//устанавливаем максимальное расстояние
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawLine (MyTransform.position, target.transform.position, Color.yellow);
		//поворачиваемся в сторону цели (устанавливаем область видимости)
		if (Vector3.Distance (target.position, MyTransform.position) < max_distance) {
			//переход из состояния покоя в бег
			animation.CrossFade ("run");
			MyTransform.rotation = Quaternion.Slerp (MyTransform.rotation, //Quaternion - работа с матрицами
		                           Quaternion.LookRotation (target.position - MyTransform.position),
		                           rotation_speed * Time.deltaTime);

			if (Vector3.Distance (target.position, MyTransform.position) >= min_distance) {
				//передвижение
				MyTransform.position += MyTransform.forward * move_speed * Time.deltaTime; 
			}
		}
		else animation.CrossFade ("idle");
	}
}
