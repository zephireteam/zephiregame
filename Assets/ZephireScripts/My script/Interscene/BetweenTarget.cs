﻿using UnityEngine;

public class BetweenTarget : MonoBehaviour {
	public GameObject player;
	public GameObject target;
	public double distance;

	private Vector3 heading;
	// Use this for initialization
	void Start () {
		heading = player.transform.position - target.transform.position;
		distance *= distance;
		Debug.Log ((heading.sqrMagnitude - distance).ToString());
	}
	
	// Update is called once per frame
	void Update () {
		heading = player.transform.position - target.transform.position;
		//Debug.Log ((heading.sqrMagnitude - distance).ToString());
		if(heading.sqrMagnitude < distance)
			Application.LoadLevel ("StartScene");
	}
}
