﻿using UnityEngine;
using System.Collections;
//драгоценности
public class Jewelry : MonoBehaviour {
	private JewelrySlot _slot;		//место складирования драгоценностей

	public Jewelry()
	{
		_slot = JewelrySlot.PocketItem;
	}

	public Jewelry(JewelrySlot slot)
	{
		_slot = slot;
	}

	public JewelrySlot Slot
	{
		get{return _slot;}
		set{_slot = value;}
	}

	public enum JewelrySlot
	{
		EarRings,		//сережки
		Necklaces,		//ожерелья
		Bracelets,		//браслеты
		Rings,			//кольца
		PocketItem		//карманные штучку
	}
}
