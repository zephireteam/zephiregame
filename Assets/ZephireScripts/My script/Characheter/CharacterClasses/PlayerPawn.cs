﻿//Класс управления характеристиками объекта сцены
using UnityEngine;
using System.Collections;

public class PlayerPawn : MonoBehaviour
{

    //стартовые параметры основных характеристик
    public int Health = 100;
    public int Energy = 100;
    public string Name = "Unnamed";
    private ActorClass _actor;

    public GameObject pc;
    private PlayerCharacter _pc;

    // Use this for initialization
    void Start()
    {
        _actor = new ActorClass();
        pc = GameObject.Find("Player");
        _pc = pc.GetComponent<PlayerCharacter>();
        _actor.Health = _pc.GetVital((int)VitalName.Energy).AdjustedBaseValue;
        Debug.Log((_actor.Health).ToString());
        _actor.Energy = Energy;
        _actor.Name = Name;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float GetHealth()
    {
        return _actor.Health;
    }

    public float GetEnergy()
    {
        return _actor.Energy;
    }

    public string GetName()
    {
        return _actor.Name;
    }

    public float GetProcHealth()
    {
        return _actor.procHealth;
    }

    public float GetProcEnergy()
    {
        return _actor.procEnergy;
    }

    public void ChangeHealth(float adj)
    {
        _actor.ChangeHealth(adj);
    }

    public void ChangeEnergy(float adj)
    {
        _actor.ChangeEnergy(adj);
    }

}
