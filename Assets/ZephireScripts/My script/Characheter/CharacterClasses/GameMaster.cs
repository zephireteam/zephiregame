﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {
	private GameObject gs;
    private GameSettings gsScript;
	// Use this for initialization
	void Start () {
		LoadCharacter();
	}
	
	public void LoadCharacter(){
		Debug.Log("Loading data...");
		GameObject gs = GameObject.Find("__GameSettings");
		GameSettings gsScript = gs.GetComponent<GameSettings>();
		gsScript.LoadCharacterData();
	}
}
