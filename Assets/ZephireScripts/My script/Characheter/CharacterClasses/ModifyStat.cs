﻿//класс модификатора атрибутов
using System.Collections;
using System.Collections.Generic;

//структура типа ModifyAttribute
public struct ModifyingAttribute{
	public Attribute attribute;
	public float ratio;			//относительные изменения атрибутов

	public ModifyingAttribute(Attribute att, float rat){
		attribute = att;
		ratio = rat;
	}
}

public class ModifyStat : BaseStat {
	private List<ModifyingAttribute> mods; //перечень атрибутов
	//которые изменяют этот модификатор
	private int modValue;	//величина, добавляемая к baseValue модификатором

	public ModifyStat(){
		mods = new List<ModifyingAttribute>();
		modValue = 0;
	}

	//добавление модификаторов с список mods
	public void AddModifier(ModifyingAttribute mod){
		mods.Add (mod);
	}

	private void CalculateModValue(){
		modValue = 0;

		if(mods.Count > 0)
			foreach(ModifyingAttribute att in mods)
				modValue += (int) (att.attribute.AdjustedBaseValue * att.ratio);
	}
	public new int AdjustedBaseValue{
		get{return BaseValue + BuffValue + modValue;}
	}

	public void Update(){
		CalculateModValue();
	}
}