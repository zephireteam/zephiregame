﻿//навыки
public class Skill : ModifyStat {
	private bool isKnown;

	public Skill(){
		isKnown = false;
		ExpToLevel = 25;
		LevelModifier = 1.1f;
	}

	public bool IsKnown{
		get{return isKnown;}
		set{isKnown = value;}
	}
}
public enum SkillName{
	Melee_Offence,
	Melee_Defence
}