﻿//Для выведения в HUD
using UnityEngine;
using System.Collections;

public class HealthEnergy : MonoBehaviour {

	private Pawn MyPlayerInfo;
	private MainHUDClass _mainHUD;
	
	private GameObject MyPlayer;
	private Pawn param;	//health_energy_regen

	//для склянок
	private float coolDown; //время между использованием
	public float TimerH; //время для склянки ХП
	public float TimerE; //время для склянки Энергии

	// Use this for initialization
	void Start () {
		//устанавливаем счетчики для склянок
		TimerH = 0;
		TimerE = 0;
		if (coolDown == 0) {
			coolDown = 60.0f;
		}

		MyPlayer = GameObject.FindGameObjectWithTag("Player");
		_mainHUD = GameObject.Find ("__MainHUD").GetComponent<MainHUDClass> ();
		if (_mainHUD == null) Debug.Log("Can't find object __MainHUD");
		//определяем Pawn от MyPlayer
		MyPlayerInfo = MyPlayer.GetComponent<Pawn>();
		//определяем energyregen
		param = (Pawn)MyPlayer.GetComponent ("Pawn");
	}
	
	// Update is called once per frame
	void Update () {
		_mainHUD.ShowPlayerEnergyBars (true, MyPlayerInfo.GetProcEnergy());
		_mainHUD.ShowPlayerHealthBars (true, MyPlayerInfo.GetProcHealth());


		//ХП
		// пауза между склянками ХП
		if (TimerH > 0)
			TimerH -= Time.deltaTime;
		//проверка на правильность
		if (TimerH < 0)
			TimerH = 0;

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
						//проверка таймера
						if (TimerH == 0) {
								HealthFlask ();
								//присваиваем таймеру кулдаун
								TimerH = coolDown;
						}
				}

		//Энергия
		// пауза между склянками Энергии
		if (TimerE > 0)
			TimerE -= Time.deltaTime;
		//проверка на правильность
		if (TimerE < 0)
			TimerE = 0;
		
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
						//проверка таймера
						if (TimerE == 0) {
								EnergyFlask ();
								//присваиваем таймеру кулдаун
								TimerE = coolDown;
						}
				}


		//регенерация энергии
		EnergyRegen ();
		//регенерация здоровья
		HealthRegen ();
		//бег
		if (Input.GetKey (KeyCode.LeftShift)&&(Input.GetKey (KeyCode.W)||Input.GetKey (KeyCode.A)||Input.GetKey (KeyCode.S)||Input.GetKey (KeyCode.D)))
					Run ();
	}

	//бег
	private void Run(){
		param.ChangeEnergy (-10.0f * Time.deltaTime);
	}

	//регенерация энергии
	private void EnergyRegen(){
		param.ChangeEnergy (2.0f * Time.deltaTime);
	}

	//регенерация эздоровья
	private void HealthRegen(){
		param.ChangeHealth (Time.deltaTime / 2.0f);
	}

	private void HealthFlask ()
	{
		param.ChangeHealth (50.0f);
	}

	private void EnergyFlask ()
	{
		param.ChangeEnergy (50.0f);	
	}

}
