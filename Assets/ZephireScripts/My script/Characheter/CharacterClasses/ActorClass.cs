﻿/// <summary>
/// Actor class
/// Класс отвечает за хранение информации о состоянии объекта сцены
/// (это состояния здоворья и степени поврежденности), а также имеет методы
/// по управлению этими параметрами
/// </summary>
using UnityEngine;
using System.Collections;

public class ActorClass{
	//переменные для хранения значений основгых параметров
	private float _health;
	private float _energy;
	private float _maxHealth;
	private float _maxEnergy;
	private string _name;

	public ActorClass(){
		_health = 100;
		_energy = 100;
		_maxHealth = _health;
		_maxEnergy = _energy;
		_name = "noname";
	}

	public float Health{
		get{return _health;}
		set{_health = value;
			_maxHealth = value;}
	}

	public float Energy{
		get{return _energy;}
		set{_energy = value;
			_maxEnergy = value;}
	}

	public string Name{
		get{return _name;}
		set{_name = value;}
	}

	public float procHealth{
		get{return _health/(float)_maxHealth;}
	}

	public float procEnergy{
		get{return _energy/(float)_maxEnergy;}
	}

	public void ChangeHealth(float adj){
		_health += adj;
		if (_health > _maxHealth)
						_health = _maxHealth;
		if (_health <= 0)
						_health = 0;
	}

	public void ChangeEnergy(float adj){
		_energy += adj;
		if (_energy > _maxEnergy)
			_energy = _maxEnergy;
		if (_energy <= 0)
			_energy = 0;
	}

}
