﻿using UnityEngine;
using System.Collections;
using System;

public class BaseCharacter : MonoBehaviour {
	private string _name;	//имя персонажа
	private int _Level;		//уровень развития персонажа
	private int _freeExp;	//опыт для распределения

	private Attribute[] _primaryAttribute;
	private Vital[] _vital;
	private Skill[] _skill;

	#region геттеры-сеттеры
	public string Name{
		get{return _name;}
		set{_name = value;}
	}
	public int Level{
		get{return _Level;}
		set{_Level = value;}
	}
	public int FreeExp{
		get{return _freeExp;}
		set{_freeExp = value;}
	}
	#endregion

	//начальные параметры
	//метод запускаемый при создании объекта
	public void Awake(){
		_name = string.Empty;
		_Level = 0;
		_freeExp = 0;

		//задание массива атрибутов
		_primaryAttribute = new Attribute[Enum.GetValues(typeof(AttributeName)).Length];
		_vital = new Vital[Enum.GetValues(typeof(VitalName)).Length];
		_skill = new Skill[Enum.GetValues(typeof(SkillName)).Length];

		//установка зависимостей атрибутов друг от друга
		SetupPrimaryAttributes();
		SetupVitals();
		SetupSkills();
	}

	public void AddExp(int exp){
		_freeExp +=exp;
		CalculateLevel();
	}

    public int GetExp()
    {
        return _freeExp;
    }


	public void CalculateLevel(){}

	private void SetupPrimaryAttributes(){
		for(int i = 0; i < _primaryAttribute.Length;i++)
			_primaryAttribute[i] = new Attribute();
	}

	private void SetupVitals(){
		for(int i = 0; i < _vital.Length; i++)
			_vital[i] = new Vital();
		SetupVitalModefiers();
	}

	private void SetupSkills(){
		for(int i = 0; i < _skill.Length;i++)
			_skill[i] = new Skill();
		SetupSkillModifiers();
	}
	#region геттеры
	public Attribute GetPrimaryAttribute(int index){return _primaryAttribute[index];}
	public Vital GetVital(int index){return _vital[index];}
	public Skill GetSkill(int index){return _skill[index];}
	#endregion
	//установка модификаторов на Vital
	public void SetupVitalModefiers(){
		//здоровье зависит от телосложения
		ModifyingAttribute healthModify = new ModifyingAttribute();
		healthModify.attribute = GetPrimaryAttribute((int)AttributeName.Constituion);
		healthModify.ratio = 0.5f;
		GetVital((int)VitalName.Health).AddModifier(healthModify);
		//установка зависимости здоровья от телосложения
		ModifyingAttribute energyModify = new ModifyingAttribute();
		energyModify.attribute = GetPrimaryAttribute((int)AttributeName.Constituion);
		energyModify.ratio = 1.0f;
		GetVital((int)VitalName.Energy).AddModifier(energyModify);
	}
	//установка модификаторов Skill от атрибутов
	private void SetupSkillModifiers(){
		//установка зависимости атаки от двух параметров -- силы и ловкости
		ModifyingAttribute MeleeOffenceModifier1 = new ModifyingAttribute();
		ModifyingAttribute MeleeOffenceModifier2 = new ModifyingAttribute();

		MeleeOffenceModifier1.attribute = GetPrimaryAttribute((int)AttributeName.Might);
		MeleeOffenceModifier1.ratio = 0.5f;

		MeleeOffenceModifier2.attribute = GetPrimaryAttribute((int)AttributeName.Agility);
		MeleeOffenceModifier2.ratio = 0.25f;
		GetSkill((int)SkillName.Melee_Offence).AddModifier(MeleeOffenceModifier1);
		GetSkill((int)SkillName.Melee_Offence).AddModifier(MeleeOffenceModifier2);
		//установка зависимости защиты от телосложения и ловкости
		ModifyingAttribute MeleeDefenceModifier1 = new ModifyingAttribute();
		ModifyingAttribute MeleeDefenceModifier2 = new ModifyingAttribute();
		
		MeleeDefenceModifier1.attribute = GetPrimaryAttribute((int)AttributeName.Constituion);
		MeleeDefenceModifier1.ratio = 0.5f;
		
		MeleeDefenceModifier2.attribute = GetPrimaryAttribute((int)AttributeName.Agility);
		MeleeDefenceModifier2.ratio = 0.25f;
		GetSkill((int)SkillName.Melee_Defence).AddModifier(MeleeDefenceModifier1);
		GetSkill((int)SkillName.Melee_Defence).AddModifier(MeleeDefenceModifier2);
	}

	//метод обноваления статистических данных
	public void StatUpdate(){
		foreach(Vital elem in _vital){elem.Update();}
		foreach(Skill elem in _skill){elem.Update();}
	}

    public void UpdateVitals()
    {
        for (int i = 0; i < _vital.Length; i++)
            _vital[i].CurrentValue = _vital[i].BaseValue;
    }
}