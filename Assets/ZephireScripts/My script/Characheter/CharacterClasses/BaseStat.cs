﻿//базовый класс параметра
public class BaseStat {
	private int baseValue;	//основное значение параметра
	private int buffValue;	//буферная переменная
	private int expToLevel;	//количество очков, которые необходимо
							//вложить для увеличения статы

	private float levelModifier;	

	private string name;

	public BaseStat(){
		//установка начального значения
		baseValue = 0;
		buffValue = 0;
		levelModifier = 0;
		expToLevel = 100;
		name = "";
	}
	#region Basic methods to design and set up values
	//определение базовых методов сеттеров и геттеров
	//действия через оболочку
	public int BaseValue {
		get { return baseValue;}
		set {baseValue = value;}
	}
	public string Name {
		get {return name;}
		set {name = value;}
	}
	public int BuffValue{
		get{ return buffValue;}
		set {buffValue = value;}
	}
	public float LevelModifier{
		get{ return levelModifier;}
		set{levelModifier = value;}
	}
	public int ExpToLevel{
		get{ return expToLevel;}
		set{expToLevel = value;}
	}
	#endregion
	//оболочка, возвращающая опыт
	private int CalculateExpToLevel(){
		return (int) (expToLevel * levelModifier);
	}
	//левелин-ап
	public void LevelUp(){
		expToLevel = CalculateExpToLevel();
		baseValue++;
	}
	//не знаю, зачем
	public int AdjustedBaseValue{
		get{return baseValue + buffValue;}
	}
}