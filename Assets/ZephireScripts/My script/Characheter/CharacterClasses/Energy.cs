﻿using UnityEngine;
using System.Collections;

public class Energy : MonoBehaviour {

	private Pawn MyPlayerInfo;
	private MainHUDClass _mainHUD;
	
	private GameObject MyPlayer;
	private Pawn er;	//energyregen


	// Use this for initialization
	void Start () {

		MyPlayer = GameObject.FindGameObjectWithTag("Player");
		_mainHUD = GameObject.Find ("__MainHUD").GetComponent<MainHUDClass> ();
		if (_mainHUD == null) Debug.Log("Can't find object __MainHUD");
		//определяем Pawn от MyPlayer
		MyPlayerInfo = MyPlayer.GetComponent<Pawn>();
		//определяем energyregen
		er = (Pawn)MyPlayer.GetComponent ("Pawn");
	}
	
	// Update is called once per frame
	void Update () {
		_mainHUD.ShowPlayerEnergyBars (true, MyPlayerInfo.GetProcEnergy());
		//регенерация энергии
		EnergyRegen ();
		//бег
		if (Input.GetKey (KeyCode.LeftShift))
					Run ();
	}

	//бег
	private void Run(){
		er.ChangeEnergy (-10.0f * Time.deltaTime);
	}

	//регенерация энергии
	private void EnergyRegen(){
		er.ChangeEnergy (2.0f * Time.deltaTime);
	}
}
