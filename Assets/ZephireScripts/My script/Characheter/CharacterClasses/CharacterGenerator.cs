﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterGenerator : MonoBehaviour {

	public GameObject PlayerCharacterPref;
    public GameObject gs;

	//определение стилей для вывода информации через GUI
	public GUIStyle PCLabelsStyle;
	public GUIStyle PCBtnStyle;
	public GUISkin PCSkin;

    //компонент GameSettings
    GameSettings gs_;

	private PlayerCharacter _toon;
	//количество стартовых поинтов фиксировано
	private const int STARTING_POINTS = 210;
	//минимальное количество харакетристик
	private const int MIN_STARTING_ATTROBUTE_VALUE = 10;
	//начальное значение параметров
	private const int STARTING_VALUE = 50;
	//количество оставшихся поинтов
	private int _pointsLeft;

	//константы которые определяют вывод парметров на экран
	private const int OFFSET_LEFT = 10;
	private const int OFFSET_TOP_NAME = 10;
	private const int RAW_HEIGHT = 30;
	private const int BUTTON_WIDTH = 30;
	private const int BUTTON_HEIGHT = 30;
	private const int OFFSET_TOP_ATTRIBUTES = OFFSET_TOP_NAME + RAW_HEIGHT + 20;
	private const int ATTRIBUTES_WIDTH = 130;
	private const int SPACE = 5;
	private const int LABEL_WIDTH = 30;
	private const int COUNT_ATTRIBUTES = 5;

	//начальная установка
	void Start() {
		//создание объекта-пустышки, которому уже назначен скрипт PlayerController
		//это префаб, на который должна быть взята ссылка
		GameObject pc = Instantiate(PlayerCharacterPref,Vector3.zero,Quaternion.identity) as GameObject;
		pc.name = "pc";
		//задание начального количества поинтов
		_pointsLeft = STARTING_POINTS;
		//инициализация объекта PlayerController
		_toon = pc.GetComponent<PlayerCharacter>();

		//назначение атрибутам первоначальных значений
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++){
			_toon.GetPrimaryAttribute(i).BaseValue = STARTING_VALUE;
			_pointsLeft -= (STARTING_VALUE - MIN_STARTING_ATTROBUTE_VALUE);
		}
		_toon.StatUpdate();
	}

	void Update(){}

	void OnGUI(){
		//вывод на экран через GUI
		GUI.skin = PCSkin;
		DisplayName();
		DisplayAttributes();
		DisplayVitals();
		//пример отключения скина;
		GUI.skin = null;
		DisplaySkills();
		DisplayPointsLeft();
		DisplayBtnSavePC();
	}
	// вывод характеристик персонажа - основной блок
	//имя персонажа
	private void DisplayName(){
		GUI.Label (new Rect (OFFSET_LEFT, OFFSET_TOP_NAME, 50, RAW_HEIGHT), "Name ", PCLabelsStyle);
		_toon.Name = GUI.TextField(new Rect(65,10,100,RAW_HEIGHT),_toon.Name);
	}

	//выводим атрибуты персонажа
	private void DisplayAttributes(){
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++){
			GUI.Label(new Rect ( OFFSET_LEFT,									//x
			                     OFFSET_TOP_ATTRIBUTES + (i * RAW_HEIGHT),		//y
			                     ATTRIBUTES_WIDTH,								//Ширина
			                     RAW_HEIGHT),									//высота
			                     ((AttributeName)i).ToString(),					//содержимое
			                     PCLabelsStyle
			                   );								//стиль
			GUI.Label(new Rect ( OFFSET_LEFT + ATTRIBUTES_WIDTH + SPACE,		//x
			     				 OFFSET_TOP_ATTRIBUTES + (i * RAW_HEIGHT),		//y
			                     LABEL_WIDTH,								    //Ширина
			                     RAW_HEIGHT),
			          _toon.GetPrimaryAttribute(i).AdjustedBaseValue.ToString()
			          );									
			//вывод кнопок + и -
			//а также реакции при их нажатии
			if(GUI.Button (new Rect (OFFSET_LEFT + ATTRIBUTES_WIDTH + SPACE + LABEL_WIDTH,
			                         OFFSET_TOP_ATTRIBUTES + (i * RAW_HEIGHT),
			                         BUTTON_WIDTH,
			                         BUTTON_HEIGHT),
			               			 "-",
			               			 PCBtnStyle))
			{//если кнопка минус нажата, то
				if(_toon.GetPrimaryAttribute(i).BaseValue > MIN_STARTING_ATTROBUTE_VALUE)
				{
					_toon.GetPrimaryAttribute(i).BaseValue--;
					_pointsLeft++;
					_toon.StatUpdate();
				}
			}
			if(GUI.Button (new Rect (OFFSET_LEFT + ATTRIBUTES_WIDTH + SPACE + LABEL_WIDTH + BUTTON_WIDTH,
			                         OFFSET_TOP_ATTRIBUTES + (i * RAW_HEIGHT),
			                         BUTTON_WIDTH,
			                         BUTTON_HEIGHT),
			               "+",
			               PCBtnStyle)){
			//если кнопка минус нажата, то
				if(_pointsLeft > 0)
				{
					_toon.GetPrimaryAttribute(i).BaseValue++;
					_pointsLeft--;
					_toon.StatUpdate();
				}
			}
		}
	}

	private void DisplayVitals(){
		for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++){
			GUI.Label(new Rect(OFFSET_LEFT,
			                   OFFSET_TOP_ATTRIBUTES + ((i+COUNT_ATTRIBUTES)*RAW_HEIGHT),
			                   ATTRIBUTES_WIDTH,
			                   RAW_HEIGHT),
			          ((VitalName)i).ToString()
			          );
			GUI.Label(new Rect(OFFSET_LEFT + ATTRIBUTES_WIDTH + SPACE,
			                   OFFSET_TOP_ATTRIBUTES + ((i+COUNT_ATTRIBUTES)*RAW_HEIGHT),
			                   ATTRIBUTES_WIDTH,
			                   RAW_HEIGHT),			
			          _toon.GetVital(i).AdjustedBaseValue.ToString()
			          );
		}
	}

	//показать skills
	private void DisplaySkills(){
		for(int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++){
			GUI.Label(new Rect(240,
			                   OFFSET_TOP_ATTRIBUTES + (i*RAW_HEIGHT),
			                   200,
			                   RAW_HEIGHT),
			          		   ((SkillName)i).ToString()
			          );
			GUI.Label(new Rect(445,
			                   OFFSET_TOP_ATTRIBUTES + (i*RAW_HEIGHT),
			                   LABEL_WIDTH,RAW_HEIGHT),
			          _toon.GetSkill(i).AdjustedBaseValue.ToString()
			          );
		}
	}
	//свободные поинты
	private void DisplayPointsLeft(){
		GUI.Label (new Rect (240, 10, 100, RAW_HEIGHT), "Points: ");
		GUI.Label (new Rect (350, 10, 50, RAW_HEIGHT), (_pointsLeft).ToString());
	}

	//сохрянаем харктеристики персонажа
	private void DisplayBtnSavePC(){
			if((_toon.Name != "") && (_pointsLeft == 0)) {
				if(GUI.Button (new Rect (Screen.width/2-100,
				                         Screen.height/3*2,
				                         200,
				                         RAW_HEIGHT), "Start adventure")){
					SavePC();
				}
			}
			else{
				GUI.Label (new Rect (Screen.width/2-100,
				                     Screen.height/3*2,
				                     200,
				                     RAW_HEIGHT), "Complete your profile","button");
			}
	}

		private void SavePC(){
			Debug.Log("Saving player");
            GameObject gs = GameObject.Find("__GameSettings");
            gs_ = gs.GetComponent<GameSettings>();
            gs_.SaveCharacterData();
            Application.LoadLevel("My Game");
		}

    private void DisplayLevelUpButtonPC(){
			if((_pointsLeft == 0)) {
				if(GUI.Button (new Rect (Screen.width/2-100,
				                         Screen.height/3*2,
				                         200,
				                         RAW_HEIGHT), "Continue adventure")){
					SavePC();
				}
			}
			else{
				GUI.Label (new Rect (Screen.width/2-100,
				                     Screen.height/3*2,
				                     200,
				                     RAW_HEIGHT), "Distribute your points","button");
			}
	}
}














