﻿//класс жизненных показателей
public class Vital : ModifyStat {
	private int currentValue;

	public Vital () {
		currentValue = 0;
		ExpToLevel = 50;
		LevelModifier = 1.1f;
	}
	//getter-setter
	public int CurrentValue{
		get{//предотвращение переполнения
			if (currentValue > AdjustedBaseValue)
				currentValue = AdjustedBaseValue;
			return currentValue;
		}
		set{ currentValue = value;}
	}
}

public enum VitalName{
	Health,		//здоровье
	Energy		//энергия
}