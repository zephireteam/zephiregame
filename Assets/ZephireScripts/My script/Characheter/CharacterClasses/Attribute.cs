﻿//класс атрибутов
public class Attribute : BaseStat {
	public Attribute(){
		ExpToLevel = 50;
		LevelModifier = 1.05f;
	}
}

public enum AttributeName {
	Might,			//сила
	Agility,		//ловкость
	Constituion,	//телосложение
	Speed,			//скорость
	Luck			//удача
}