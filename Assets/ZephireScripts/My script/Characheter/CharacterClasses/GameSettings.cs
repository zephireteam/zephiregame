﻿/*
 Информация по загрузке и сохранению параметров:
 
 Чтобы получить доступ к сохранённым данным на Виндовс, нужно получить доступ
к реестрну. Для этого вызываем программу REGEDIT.exe, которую можно запустить
через поисковик.
Войдя в реестр ищем путь HKCU\Software\[company name]\[product name],
где HKCU - HOT KEY CURRENT USER
[company name], [product name] - данные свойст проекта в разделе Player

Размер файла не должен превышать 1 мегабайт
 */

using UnityEngine;
using System.Collections;
using System;

public class GameSettings : MonoBehaviour {
	
	void Awake(){
		//делаем объект переходящим из сцены в сцену
		DontDestroyOnLoad(this);
	}
	
	public void SaveCharacterData(){
		Debug.Log("Saving data...");
		//получаем ссылку на обект с именем pc и используем его компонент
		GameObject pc = GameObject.Find("pc");
		PlayerCharacter pcClass = pc.GetComponent<PlayerCharacter>();
		//удаляем все сохраненные данны в реестре перед новой записью
		PlayerPrefs.DeleteAll();
		
		//сохраняем имя персонажа при помощи спец функции в спец месте (лол што)
		//место хранения указано в заголовке скрипта
		PlayerPrefs.SetString("Player Name",pcClass.Name);
		
		//вывод атрибутов Attribute;
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++){
			PlayerPrefs.SetInt(((AttributeName)i).ToString() + " - BaseValue",
                                pcClass.GetPrimaryAttribute(i).AdjustedBaseValue);
			PlayerPrefs.SetInt(((AttributeName)i).ToString() + " - ExpToLevel",
								pcClass.GetPrimaryAttribute(i).ExpToLevel);
		}
		
		//вывод атрибутов Vitals
		for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++){
			PlayerPrefs.SetInt(((VitalName)i).ToString() + " - BaseValue",
                                pcClass.GetVital(i).AdjustedBaseValue);
			PlayerPrefs.SetInt(((VitalName)i).ToString() + " - ExpToLevel",
								pcClass.GetVital(i).ExpToLevel);
			//PlayerPrefs.SetInt(((VitalName)i).ToString() + " - CurrentValue",
			//					pcClass.GetVital(i).CurrentValue);					
		}
		
		//вывод атрибутов Skill
		for(int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++){
			PlayerPrefs.SetInt(((SkillName)i).ToString() + " - BaseValue",
                                pcClass.GetSkill(i).AdjustedBaseValue);
			PlayerPrefs.SetInt(((SkillName)i).ToString() + " - ExpToLevel",
								pcClass.GetSkill(i).ExpToLevel);
		}
	}
	
	public void LoadCharacterData(){
		Debug.Log("Loading data...");
		//получаем ссылку на объект с именем pc
		GameObject pc = GameObject.Find("Player");
		PlayerCharacter pcClass = pc.GetComponent<PlayerCharacter>();
		
		Debug.Log("Player Name: " + (pcClass.Name).ToString());
		
		//получение атрибутов Attribute
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++){
			pcClass.GetPrimaryAttribute(i).BaseValue = PlayerPrefs.GetInt(((AttributeName)i).ToString() + " - BaseValue", 0);
            Debug.Log(((AttributeName)i).ToString() + " - BaseValue = " + pcClass.GetPrimaryAttribute(i).BaseValue);
			pcClass.GetPrimaryAttribute(i).ExpToLevel = PlayerPrefs.GetInt(((AttributeName)i).ToString() + " - ExpToLevel", 0);
            Debug.Log(((AttributeName)i).ToString() + " - ExpToLevel = " + pcClass.GetPrimaryAttribute(i).ExpToLevel);
		}

        //получение виталов
        for (int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
        {
            pcClass.GetVital(i).BaseValue = PlayerPrefs.GetInt(((VitalName)i).ToString() + " - BaseValue", 0);
            Debug.Log(((VitalName)i).ToString() + " - BaseValue = " + pcClass.GetVital(i).BaseValue);
            pcClass.GetVital(i).ExpToLevel = PlayerPrefs.GetInt(((VitalName)i).ToString() + " - ExpToLevel", 0);
            Debug.Log(((VitalName)i).ToString() + " - ExpToLevel = " + pcClass.GetVital(i).ExpToLevel);
            //pcClass.GetPrimaryAttribute(i).ExpToLevel = PlayerPrefs.GetInt(((VitalName)i).ToString() + " - CurrentValue", 0);
            //Debug.Log(((VitalName)i).ToString() + " - CurrentValue = " + pcClass.GetPrimaryAttribute(i).ExpToLevel);
        }

        //получение атаки и защиты
        for (int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++)
        {
            pcClass.GetSkill(i).BaseValue = PlayerPrefs.GetInt(((SkillName)i).ToString() + " - BaseValue", 0);
            Debug.Log(((SkillName)i).ToString() + " - BaseValue = " + pcClass.GetSkill(i).BaseValue);
            pcClass.GetSkill(i).ExpToLevel = PlayerPrefs.GetInt(((SkillName)i).ToString() + " - ExpToLevel", 0);
            Debug.Log(((SkillName)i).ToString() + " - ExpToLevel = " + pcClass.GetSkill(i).ExpToLevel);
        }
	}
}
