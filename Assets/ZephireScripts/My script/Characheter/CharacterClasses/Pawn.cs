﻿//Класс управления характеристиками объекта сцены
using UnityEngine;
using System.Collections;

public class Pawn : MonoBehaviour {
    public bool isPlayer = false;
	//стартовые параметры основных характеристик
	public int Health = 100;
	public int Energy = 100;
	public string Name = "Unnamed";
	private ActorClass _actor;
    private GameObject player;
    private PlayerCharacter pc;
	// Use this for initialization
	void Start () {
		_actor = new ActorClass ();
		_actor.Health = Health;
		_actor.Energy = Energy;
		_actor.Name = Name;
        if (isPlayer) {
            player = GameObject.Find("Player");
            pc = player.GetComponent<PlayerCharacter>();
            //pc.UpdateVitals();
            _actor.Health = pc.GetVital(0).BaseValue;
            _actor.Energy = pc.GetVital(1).BaseValue;
            Debug.Log("Actor healts is " + _actor.Health);
            Debug.Log("Actor energy is " + _actor.Energy);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public float GetHealth(){
		return _actor.Health;
	}

	public float GetEnergy(){
		return _actor.Energy;
	}

	public string GetName(){
		return _actor.Name;
	}

	public float GetProcHealth(){
		return _actor.procHealth;
	}

	public float GetProcEnergy(){
		return _actor.procEnergy;
	}

	public void ChangeHealth(float adj){
		_actor.ChangeHealth (adj);
	}

	public void ChangeEnergy(float adj){
		_actor.ChangeEnergy (adj);
	}

}
