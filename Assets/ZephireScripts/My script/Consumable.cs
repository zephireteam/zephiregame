﻿using UnityEngine;
using System.Collections;
//предметы потребления
public class Consumable : BuffItem {
	private Vital[] _vital;			//здоровье и мана
	private int[] _amountToHeal;	//лечебные порции

	private float _buffTime;

	public Consumable()
	{
		Reset ();
	}

	public Consumable(Vital[] v, int[] a, float b)
	{
		_vital = v;
		_amountToHeal = a;
		_buffTime = b;
	}

	public void Reset()
	{
		_buffTime = 0;
		for (int cnt = 0; cnt < _vital.Length;cnt++)
		{
			_vital[cnt] = new Vital();
			_amountToHeal[cnt] = 0;
		}
	}

	public int VitalCount()
	{
		return _vital.Length;
	}

	public Vital VitalAtIndex(int index)
	{
		//защита от неправильного индекса
		if (index < _vital.Length && index > -1)
						return _vital [index];
		else
			return new Vital();
	}

	public int HealAtIndex(int index)
	{
		//защита от неправильного индекса
		if (index < _amountToHeal.Length && index > -1)
			return _amountToHeal [index];
		else
			return 0;
	}

	public void SetVitalAt(int index, Vital vital)
	{
		//защита от неправильного индекса
		if (index < _vital.Length && index > -1)
			_vital [index] = vital;
	}

	public void SetHealAt(int index, int heal)
	{
		//защита от неправильного индекса
		if (index < _amountToHeal.Length && index > -1)
			_amountToHeal [index] = heal;
	}

	public void SetVitalAndHealAtIndex(int index, Vital vital, int heal)
	{
		SetVitalAt (index, vital);
		SetHealAt (index, heal);
	}

	public float BuffTimer
	{
		get{return _buffTime;}
		set{_buffTime = value;}
	}
}
