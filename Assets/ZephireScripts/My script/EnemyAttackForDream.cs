﻿using UnityEngine;
using System.Collections;

public class EnemyAttackForDream : MonoBehaviour {
	
	public GameObject target;	//цель Enemy

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
			Attack();
		}

	// атака
	private void Attack(){
		//Вводим переменную distance - расстояние между player и enemy
		float distance = Vector3.Distance (target.transform.position, transform.position);
		//Вычисляем единичный вектор направления к цели
		Vector3 ort = (target.transform.position - transform.position).normalized;
		//Вычисляем нахождение цели в поле зрения: значение -1 - сзади
		//значение 1 - cпереди. 0 - сбоку. Значения меняются от -1 до 1
		float direction = Vector3.Dot(ort, transform.forward);
		//если direction = 0, то угол 180, если direction = 0.5, то угол 90 градусов, причем \/
		if (distance < 1 && direction > 0) {
			Application.LoadLevel("Dream");
		}
	}
}
