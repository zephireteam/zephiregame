﻿using UnityEngine;
using System.Collections;

public class PlayerAttackExt : MonoBehaviour {
	
	public GameObject target;	//цель игрока
	public float coolDown; //время между атаками
	public float attackTimer; //время проведения атаки

	private GameObject MyPlayer;

	private Pawn ce;
	
	
	// Use this for initialization
	void Start () {
		attackTimer = 0;
		if (coolDown == 0) {
			coolDown = 2.0f;
		}
		MyPlayer = GameObject.FindGameObjectWithTag("Player");
		//определяем Pawn для игрока
		ce = (Pawn)MyPlayer.GetComponent ("Pawn");
	}
	
	// Update is called once per frame
	void Update () {

				// пауза между ударами
				if (attackTimer > 0)
						attackTimer -= Time.deltaTime;
				//проверка на правильность
				if (attackTimer < 0)
						attackTimer = 0;
		//проверка на наличие энергии для удара
		if (ce.GetEnergy() > 10.0f) {
						//нажатие на ЛКМ
						if (Input.GetMouseButtonUp (0)) {
								//проверка таймера
								if (attackTimer == 0) {
										Attack ();
										//присваиваем таймеру кулдаун
										attackTimer = coolDown;
								}

						}
				}
		}
	// атака
	private void Attack(){
		if (target != null) {
						//Вводим переменную distance - расстояние между player и enemy
						float distance = Vector3.Distance (target.transform.position, transform.position);
						//Вычисляем единичный вектор направления к цели
						Vector3 ort = (target.transform.position - transform.position).normalized;
						//Вычисляем нахождение цели в поле зрения: значение -1 - сзади
						//значение 1 - cпереди. 0 - сбоку. Значения меняются от -1 до 1
						float direction = Vector3.Dot (ort, transform.forward);
						//если direction = 0, то угол 180, если direction = 0.5, то угол 90 градусов, причем \/
						if (distance < 2 && direction > 0) {
								//У цели Enemy ищем компонент с именем Pawn - скрипт
								Pawn pw = (Pawn)target.GetComponent ("Pawn");
								//используем метод изменения состояния здоровья
								pw.ChangeHealth (-10.0f);
								GetComponent<TargettingExt> ().SelectTarget ();
								if (pw.GetHealth() == 0.0f){
									Destroy(target);
                                    //начисление опыта
                                    GameObject pc = GameObject.Find("Player");
                                    PlayerCharacter pc_ = pc.GetComponent<PlayerCharacter>();
                                    pc_.AddExp(10);
                                    Debug.Log("Current exp is ");
								}
						}
						//изменение энергии
						ce.ChangeEnergy (-10.0f);
						
				}
	}
}
