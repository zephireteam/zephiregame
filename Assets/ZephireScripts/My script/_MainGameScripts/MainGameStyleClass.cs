﻿using UnityEngine;
using System.Collections;

public class MainGameStyleClass : MonoBehaviour {

	public GameObject MainHUDPref;
	//ссылка на игровой интерфейс
	private GameObject _GameHUD;

	// Use this for initialization
	void Start () {
		Screen.showCursor = false;
		_GameHUD = Instantiate (MainHUDPref, Vector3.zero, Quaternion.identity) as GameObject;
		_GameHUD.name = "__MainHUD";
	}

}
