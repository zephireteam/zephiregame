﻿//выбор цели
using UnityEngine;
using System.Collections;
//устанавливаем дополнительную систему для возможности работы с листингами
using System.Collections.Generic;

public class Targetting : MonoBehaviour {
	//хранилище всех врагов
	public List <Transform> targets;

	//внутренняя переменная для хранения текущей цели
	public Transform selectedTarget;

	private Transform MyTransform;

	// Use this for initialization
	void Start () {
		//назначаем пустой листинг
		targets = new List<Transform> ();
		MyTransform = transform;
		AddAllEnemies ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Tab)) {
			//выбираем цвет Tabом
			TargetEnemy();
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			DeSelectTarget();
			selectedTarget = null;
		}
	}

	//поиск и добавление все противников в список
	public void AddAllEnemies(){
		//помещаем всех врагов в массив go
		GameObject[] go = GameObject.FindGameObjectsWithTag("Enemy");
		//каждый элемент из найденных заносим в массив потенциальных целей
		foreach (GameObject enemy in go) 
			AddTarget(enemy.transform);
	}

	//добавление в массив очередного элемента
	public void AddTarget(Transform enemy){
		targets.Add (enemy);
	}

	//выбор конкретной цели из списка
	private void TargetEnemy(){
		//если ни один не был выбран, выбираем наиближайщий
		if (selectedTarget == null){
			SortTargetByDistance();
			selectedTarget = targets[0];
		}
		else {
			//иначе - перебор от первого до последнего
			int index = targets.IndexOf(selectedTarget);
			if (index < targets.Count-1){
				index++;
			}
			else {
				index = 0;
			}
			//снимаем пометку с текущей цели
			DeSelectTarget();
			//выбираем следующую
			selectedTarget = targets[index];
			//метим ее цветом

		}
		SelectTarget();
	}

	//Сортировка списка по расстоянию до игрока
	private void SortTargetByDistance(){
		targets.Sort (delegate(Transform t1, Transform t2) {
			//сравнивается расстояние между объектом t1 и игроком и расстояние между объектом t2 и игроком
			return Vector3.Distance(t1.position,MyTransform.position).CompareTo(Vector3.Distance(t2.position,MyTransform.position));
		});
	}

	//метим выбранный элемент цветом
	private void SelectTarget(){
		selectedTarget.FindChild("Plane").renderer.material.color = Color.white;
		//ищем компонент PlayerAttack, который находится на этом же объекте
		PlayerAttack pa = (PlayerAttack)GetComponent ("PlayerAttack");
		//и назначаем цели атаки другую цель или новую
		pa.target = selectedTarget.gameObject;
		//активизируем бар здоровья у Enemy объекта
		EnemyHealth eh = (EnemyHealth) selectedTarget.gameObject.GetComponent("EnemyHealth");
		eh.ShowHealthBar ();
	}

	//убираем пометку выбранной цели
	private void DeSelectTarget(){
		selectedTarget.FindChild("Plane").renderer.material.color = Color.clear;
		//selectedTarget.renderer.material.color = Color.white;
		//деактивируем отображения бара здоровья
		EnemyHealth eh = (EnemyHealth) selectedTarget.gameObject.GetComponent("EnemyHealth");
		eh.HideHealthBar ();
	}
}