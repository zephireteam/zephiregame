﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	public int max_health = 100;
	//меняющееся здоровье
	private int	dinam_health;
	//длина бара здоровья
	private float healthBarLenth;

	// Use this for initialization
	void Start () {
		//начальная ширина бара
		healthBarLenth = Screen.width / 8;
		//проверка на правильность значений
		if (max_health < 1)
						max_health = 1;
		//первичное значение здоровья присваиваем максимальному
		dinam_health = max_health;
	}

	//бар чере графический интерфейс
	void OnGUI(){
		//бар здоровья со значениями
		GUI.Box(new Rect (20,Screen.height - 50,healthBarLenth,20),dinam_health + "/" + max_health);
	}

	//Метод изменения здоровья извне(другими факторами)
	public void Change_dinam_health(int param){
		dinam_health += param; // можно заменить на +=
		//проверка корректности значений dinam_health
		if (dinam_health < 0)
						dinam_health = 0;
		if (dinam_health > max_health)
						dinam_health = max_health;
		//изменение статус-бара
 		healthBarLenth = (Screen.width / 8) * (dinam_health / (float)max_health);
	}

	// Update is called once per frame
	void Update () {
		//Change_dinam_health (dinam_health);
	}
}
