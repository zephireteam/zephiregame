﻿using UnityEngine;
using System.Collections;


public class MainHUDClass : MonoBehaviour {
	
	//все имеющиеся бары в HUD
	private GUITexture[] _listGUITextures;	//указатели на текстуры
	//начальные параметры баров и их положения
	private Rect[] _dispRects;	//стартовые значения текстур

	// Use this for initialization
	//определение начальных координат
	void Start () {
		_listGUITextures = GetComponentsInChildren<GUITexture> ();
		_dispRects = new Rect[_listGUITextures.Length];
		if (_listGUITextures.Length > 0) {
			for (int i = 0; i < _listGUITextures.Length;i++ )
				_dispRects[i] = _listGUITextures[i].pixelInset;
		}
		else{
			Debug.Log("GUITextures not found");
	}

	//выводим бары
	ShowEnemyHealthBars(false,1);
	ShowPlayerHealthBars(true,1);
	ShowPlayerEnergyBars(true,1);
}

	//Переключатель показа бара здоровья Enemy
	//b - состояние показа бара(активен/не активен)
	//health - процент состояния здоровья
	public void ShowEnemyHealthBars(bool b, float health){
		if(b){
			//показать бары
			if(_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "EnemyHealthBarBorder" ||
					    _listGUITextures[i].name == "EnemyHealthBarBody"){
						if (_listGUITextures[i].name == "EnemyHealthBarBody"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          Screen.width / 2 - 128,
							                                          Screen.height - 52,
							                                          (int)(_dispRects[i].width*health),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
						if (_listGUITextures[i].name == "EnemyHealthBarBorder"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          Screen.width / 2 - 128,
							                                          Screen.height - 52,
							                                          (int)(_dispRects[i].width),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
					}
				}
			}//if(_listGUITextures)

		}//if(b)
		else {
			//скрыть бары
			if (_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "EnemyHealthBarBorder" ||
					    _listGUITextures[i].name == "EnemyHealthBarBody"){
						_listGUITextures[i].pixelInset = new Rect(0,0,0,0);
					}
				}
			}
		}
	}//public

	//Переключатель показа бара здоровья Player
	//b - состояние показа бара(активен/не активен)
	//health - процент состояния здоровья
	public void ShowPlayerHealthBars(bool b, float health){
		if(b){
			//показать бары
			if(_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "PlayerHealthBarBorder" ||
					    _listGUITextures[i].name == "PlayerHealthBarBody"){
						if (_listGUITextures[i].name == "PlayerHealthBarBody"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          25,
							                                          5,
							                                          (int)(_dispRects[i].width*health),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
						if (_listGUITextures[i].name == "PlayerHealthBarBorder"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          25,
							                                          5,
							                                          (int)(_dispRects[i].width),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
					}
				}
			}//if(_listGUITextures)
			
		}//if(b)
		else {
			//скрыть бары
			if (_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "PlayerHealthBarBorder" ||
					    _listGUITextures[i].name == "PlayerHealthBarBody"){
						_listGUITextures[i].pixelInset = new Rect(0,0,0,0);
					}
				}
			}
		}
	}//public

	//Переключатель показа бара энергии Player
	//b - состояние показа бара(активен/не активен)
	//health - процент состояния здоровья
	public void ShowPlayerEnergyBars(bool b, float energy){
		if(b){
			//показать бары
			if(_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "PlayerEnergyBarBorder" ||
					    _listGUITextures[i].name == "PlayerEnergyBarBody"){
						if (_listGUITextures[i].name == "PlayerEnergyBarBody"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          Screen.width - 281,
							                                          5,
							                                          (int)(_dispRects[i].width*energy),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
						if (_listGUITextures[i].name == "PlayerEnergyBarBorder"){
							_listGUITextures[i].pixelInset = new Rect(//_dispRects[i].x,
							                                          //_dispRects[i].y,
							                                          Screen.width - 281,
							                                          5,
							                                          (int)(_dispRects[i].width),
							                                          _dispRects[i].height);
						}
						//else{
						//	_listGUITextures[i].pixelInset = _dispRects[i];
						//}
					}
				}
			}//if(_listGUITextures)
			
		}//if(b)
		else {
			//скрыть бары
			if (_listGUITextures.Length > 0){
				for (int i = 0; i < _listGUITextures.Length; i++){
					if (_listGUITextures[i].name == "PlayerEnergyBarBorder" ||
					    _listGUITextures[i].name == "PlayerEnergyBarBody"){
						_listGUITextures[i].pixelInset = new Rect(0,0,0,0);
					}
				}
			}
		}
	}//public



}
