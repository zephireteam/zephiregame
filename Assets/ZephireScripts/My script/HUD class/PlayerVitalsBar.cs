﻿//Этот класс описывает работу характеристик здоровья и
//жизненных показателей объекта, на котором установлен
using UnityEngine;
using System.Collections;

public class PlayerVitalsBar : MonoBehaviour {
	//какой бар привязан
	public GameObject VitalBarPref;

	//размер бара при 100% здоровья
	private int _maxBarLength;
	//текущий размер бара
	private int _dinBarLength;
	//Ссылка на бар здоровья игрока
	private GameObject _vb;
	//ссылка на картинку здоровья в этом баре 
	private GUITexture _plGUIBar;

	void Start(){
		_vb = Instantiate (VitalBarPref, Vector3.zero, Quaternion.identity) as GameObject;
		_vb.name = "PlayerVitalBar";
		GameObject go = GameObject.Find("PlayerVitalBar/HealthBarBody");
		if (go != null) {
			_plGUIBar = go.gameObject.GetComponent<GUITexture>();
			if (_plGUIBar != null){
				//снимаем показатели текущего состояния бара здоровья
				//в качестве максимума
				_maxBarLength = (int)_plGUIBar.pixelInset.width;
				//и текущего состояния здоровья
				_dinBarLength = _maxBarLength;
				//инициализируем вывод бара внутри класса
				onChangeHealthBarSize(100,100);
			}
			else Debug.Log("GUI Texture not found");
		}
		else Debug.Log("Bar not found");

		OnEnable ();
	}

	//Вызывается в момент появления или генерации объекта на сцене
	public void OnEnable()
	{
		Messenger<int,int>.AddListener ("Health change", onChangeHealthBarSize);
	}

	//Вызывается в момент удаления объекта со сцены при помощи Destroy
	//или выхода из игры
	public void OnDisable()
	{
		Messenger<int,int>.RemoveListener ("Health change", onChangeHealthBarSize);
	}

	//изменение размера бара в зависимости от соотношения
	//максимального и текущего здоровья
	public void onChangeHealthBarSize(int dinHealth, int maxHealth){
		Debug.Log("Прием события на изменение бара с параметрами...");
		Debug.Log("dinHealth = " + dinHealth + "maxHealth = " + maxHealth);
		//защита от неправильного соотношения максимума и текущего здоровья
		if (dinHealth > maxHealth)
						dinHealth = maxHealth;
		_dinBarLength = (int)((dinHealth / (float)maxHealth) * _maxBarLength);
		Debug.Log ("_dinBarLength = " + _dinBarLength);
		_plGUIBar.pixelInset = new Rect(_plGUIBar.pixelInset.x,
									 	_plGUIBar.pixelInset.y,
										(float)_dinBarLength,
									 	_plGUIBar.pixelInset.height);
	}
}
