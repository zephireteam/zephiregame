﻿using UnityEngine;
using System.Collections;
//драгоценности
public class Clothing : BuffItem {
	private ArmorSlot _slot;		//слот для хранения брони
	
	public Clothing()
	{
		_slot = ArmorSlot.Head;
	}
	
	public Clothing(ArmorSlot slot)
	{
		_slot = slot;
	}
	
	public ArmorSlot Slot
	{
		get{return _slot;}
		set{_slot = value;}
	}
	
	public enum ArmorSlot
	{
		Head,			//голова
		Shoulders,		//наплечники
		UpperBody,		//нательная броня
		Torso,			//торс
		Legs,			//ноги
		Hands,			//руки
		Feet,			//ступни
		Back			//спина
	}
}