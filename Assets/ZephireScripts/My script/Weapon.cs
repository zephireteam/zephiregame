﻿using UnityEngine;
using System.Collections;

public class Weapon : BuffItem {
	private int _maxDamage;			//максимальное повреждение
	private float _dmgVar;			//повреждение
	private float _maxRange;		//максимальная дистанция
	private DamageType _dmgType;	//тип повреждений

	public Weapon()
	{
		_maxRange = 0;
		_maxDamage = 0;
		_dmgVar = 0;
		_dmgType = DamageType.Bludgeon;
	}

	public Weapon(int mDmg, float dmgV, float mRange, DamageType dt)
	{
		_maxRange = mRange;
		_maxDamage = mDmg;
		_dmgVar = dmgV;
		_dmgType = dt;
	}

	public int MaxDamage
	{
		get{return _maxDamage;}
		set{_maxDamage = value;}
	}

	public float DamageVar
	{
		get{return _dmgVar;}
		set{_dmgVar = value;}
	}

	public float MaxRange
	{
		get{return _maxRange;}
		set{_maxRange = value;}
	}

	public DamageType TypeOfDamage
	{
		get{return _dmgType;}
		set{_dmgType = value;}
	}

	public enum DamageType
	{
		Bludgeon,		//дробящее
		Pierce,			//колющее
		Slash,			//режущее
		Fire,			//огонь
		Ice,			//лед
		Lighting,		//световое
		Acid			//кислотное
	}
}
