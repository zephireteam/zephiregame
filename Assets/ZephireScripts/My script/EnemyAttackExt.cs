﻿using UnityEngine;
using System.Collections;

public class EnemyAttackExt : MonoBehaviour {
	
	public GameObject target;	//цель Enemy
	public float coolDown; //время между атаками
	public float attackTimer; //время проведения атаки

	public Pawn targInfo;

	// Use this for initialization
	void Start () {
		attackTimer = 0;
		if (coolDown == 0) {
			coolDown = 2.0f;
		}
		target = GameObject.FindGameObjectWithTag("Player");
		targInfo = target.GetComponent<Pawn>();
	}
	
	// Update is called once per frame
	void Update () {
		// пауза между ударами
		if (attackTimer > 0)
			attackTimer -= Time.deltaTime;
		//проверка на правильность
		if (attackTimer < 0)
			attackTimer = 0;
		//проверка таймера
		if (attackTimer == 0){
			Attack();
			//присваиваем таймеру кулдаун
			//перенесли attackTimer = coolDown в метод Attack
		}
	}
	// атака
	private void Attack(){
		//Вводим переменную distance - расстояние между player и enemy
		float distance = Vector3.Distance (target.transform.position, transform.position);
		//Вычисляем единичный вектор направления к цели
		Vector3 ort = (target.transform.position - transform.position).normalized;
		//Вычисляем нахождение цели в поле зрения: значение -1 - сзади
		//значение 1 - cпереди. 0 - сбоку. Значения меняются от -1 до 1
		float direction = Vector3.Dot(ort, transform.forward);
		//если direction = 0, то угол 180, если direction = 0.5, то угол 90 градусов, причем \/
		if (distance < 2 && direction > 0) {
			//У цели Player ищем компонент с именем Pawn - скрипт
			Pawn pw = (Pawn)target.GetComponent ("Pawn");
			//используем метод изменения состояния здоровья
			pw.ChangeHealth (-10.0f);
			attackTimer = coolDown;
		}
	}
}
