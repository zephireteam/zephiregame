﻿using UnityEngine;

public class Item : MonoBehaviour {
	private string _name;
	private	int _value;
	private	RarityTypes _rarity;	//тип ценности предмета
	private	int _dinDur;			//текущая долговечность
	private	int _maxDur;			//максимальная долговечность

	//пустой конструктор
	public Item(){
		_name = "Need Name";
		_value = 0;
		_rarity = RarityTypes.Common;
		_maxDur = 50;
		_dinDur = _maxDur;
	}

	//конструктор, предусматривающий передачу данных настройки
	public Item(string name, int value, RarityTypes rare, int maxDur, int dinDur){
		_name = name;
		_value = value;
		_rarity = rare;
		_maxDur = maxDur;
		_dinDur = dinDur;
	}

	public string Name {
		set{_name = value;}
		get{return _name;}
	}

	
	public int Value {
		set{_value = value;}
		get{return _value;}
	}
	
	public RarityTypes Rare {
		set{_rarity = value;}
		get{return _rarity;}
	}
	
	public int MaxDurability {
		set{_maxDur = value;}
		get{return _maxDur;}
	}

	public int DinDurability {
		set{_dinDur = value;}
		get{return _dinDur;}
	}

	public enum RarityTypes{
		Common,			//обычные
		Uncommon,		//необычные
		Rare			//редкие
	}
}