﻿using UnityEngine;
using System.Collections;

public class MainMenu : ScriptableObject {
	public bool isNewGame = false;
	public bool isLoadGame = false;
	public bool isExit = false;

	void OnMouseEnter() {
		renderer.material.color = Color.grey;
	}
	void OnMouseExit() {
		renderer.material.color = Color.white;
	}
	void OnMouseUp() {
		if (isNewGame) {Application.LoadLevel("My Game");}
		if (isLoadGame) {} //реализовать сохрание игры
		if (isExit) {Application.Quit();}
	}
	void Start () {
		renderer.material.color = Color.white;
	}
}
